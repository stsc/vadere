package org.vadere.simulator.projects.dataprocessing.outputfile;

import org.vadere.simulator.projects.dataprocessing.datakey.TimestepKey;

/**
 * @author Mario Teixeira Parente
 *
 */

public class TimestepOutputFile extends OutputFile<TimestepKey> {

    public TimestepOutputFile() {
        super("hdfkh");
    }
}
