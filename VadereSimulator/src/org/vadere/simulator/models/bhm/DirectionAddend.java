package org.vadere.simulator.models.bhm;

import org.vadere.util.geometry.shapes.VPoint;

public interface DirectionAddend {
	public VPoint getDirectionAddend();
}
