package org.vadere.state.attributes.processor;

/**
 * @author Mario Teixeira Parente
 *
 */

public class AttributesEvacuationTimeProcessor extends AttributesProcessor {
    private int pedestrianEvacuationTimeProcessorId;

    public int getPedestrianEvacuationTimeProcessorId() {
        return this.pedestrianEvacuationTimeProcessorId;
    }
}
