package org.vadere.state.attributes.processor;

public class AttributesFloorFieldProcessor extends AttributesProcessor {
    private int targetId;
	private double resolution;

    public int getTargetId() {
        return this.targetId;
    }

	public double getResolution() {
		return resolution;
	}
}
